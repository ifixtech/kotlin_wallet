package com.pylons.wallet.core.types

enum class Backend {
    DUMMY,
    ALPHA_REST
}