package com.pylons.wallet.core.constants

class Keys {
    companion object {
        const val coins = "coins"
        const val error = "error"
        const val errorCode = "errorCode"
        const val id = "id"
        const val info = "info"
        const val items = "items"
        const val name = "name"
        const val profileExists = "profileExists"
        const val pylons = "pylons"
        const val success = "success"
        const val otherProfileId = "otherProfileId"
        const val coinsIn = "coinsIn"
        const val coinsOut = "coinsOut"
        const val itemsIn = "itemsIn"
        const val itemsOut = "itemsOut"
        const val recipe = "recipe"
        const val cookbook = "cookbook"
        const val preferredItemIds = "preferredItemIds"
        const val strings = "strings"
        const val json = "json"
        const val friends = "friends"
    }
}