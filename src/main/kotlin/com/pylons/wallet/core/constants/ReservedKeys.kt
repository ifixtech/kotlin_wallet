package com.pylons.wallet.core.constants

class ReservedKeys {
    companion object {
        const val prefix = "@P__"
        const val wcAction = "${prefix}action"
        const val itemName = "${prefix}itemName"
        const val profileName = "${prefix}profileName"
    }
}