package com.pylons.wallet.core.constants

class LogTag {
    companion object {
        const val info = "INFO"
        const val coreError = "CORE_ERROR"
        const val walletError = "WALLET_ERROR"
        const val externalError = "EXTERNAL_ERROR"
    }
}